# somina
[![Build Status](https://travis-ci.org/tangerinetulip/somina.svg?branch=master)](https://travis-ci.org/tangerinetulip/somina) [![Go Report Card](https://goreportcard.com/badge/github.com/tangerinetulip/somina)](https://goreportcard.com/report/github.com/tangerinetulip/somina)
### My Discord bot for posting images from Twitter, with facial recognition.

Still in alpha stage. Kind of works. Check issues for problems.

___

Uses:
- [TOML](https://github.com/BurntSushi/toml)
- [Anaconda](https://github.com/tangerinetulip/anaconda)
- [DiscordGo](https://github.com/bwmarrin/discordgo)
- [GoRequest](https://github.com/parnurzeal/gorequest)
- [kairos-sdk-go](https://github.com/tangerinetulip/kairos-sdk-go)
- [goimagehash](https://github.com/tangerinetulip/goimagehash)
- [BuntDB](https://github.com/tidwall/buntdb/)
- [Logrus](https://github.com/sirupsen/logrus)
- [Raven-Go](https://github.com/getsentry/raven-go)
