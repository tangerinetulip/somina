/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

import (
	"io"
	"net/http"
	"os"
	"path"
	"regexp"

	"github.com/parnurzeal/gorequest"
	"github.com/sirupsen/logrus"
	"github.com/tidwall/buntdb"
)

var (
	buntConf buntdb.Config
	smDB     buntdb.DB
)

func init() {
	logrus.Debug("Initializing Bunt database...")

	if err := smDB.ReadConfig(&buntConf); err != nil {
		logrus.Fatal("Failed to read Bunt config!")
	}

	buntConf.SyncPolicy = buntdb.Always

	if err := smDB.SetConfig(buntConf); err != nil {
		logrus.Fatal("Failed to write Bunt config!")
	}
}

// DownloadImage downloads the given image and saves according to subject
func DownloadImage(url string, accountOrTag string, subject string) {
	request := gorequest.New()

	resp, _, errs := request.Get(url).End()
	for _, errRequest := range errs {
		logrus.Error("Failed to request a given URL.")
		errHelp(errRequest)
	}

	header := resp.Header.Get("Content-Disposition")
	fileExt := regexp.MustCompile(`[^"]+\.[^\\"]{3,4}`)
	filename := fileExt.FindString(header)

	basePath := GetDownloadBasePath()

	switch config.Bot.UseFallback {
	case true:
		// Taken from kairos.go's useFallback
		for _, gallery := range galleries.Gallery {
			for _, subject := range gallery.Subjects {
				fallbackDownload(subject, accountOrTag, basePath, filename, resp)
			}
		}
	case false:
		noFallbackDownload(subject, filename, basePath, resp)
	}

	logrus.WithFields(logrus.Fields{
		"Subject":  subject,
		"Filename": filename}).Info("Downloading image...")
}

// fallbackDownload gets the file and puts it into the appropriate subject folder
// This is basically just to reduce the cyclomatic complexity of DownloadImage
func fallbackDownload(subject Subject, accountOrTag string, basePath string, filename string, resp *http.Response) {
	twit := GetTwitterClient()

	for _, list := range subject.Lists {
		users, errGetList := twit.GetListMembers(list, nil)
		if errGetList != nil {
			logrus.Error("Failed to get Twitter list members.")
		}
		errHelp(errGetList)

		for _, user := range users {
			if user.IdStr == accountOrTag {
				createFile(basePath+subject.Name+"/", filename, resp)
			}
		}
	}

	for _, user := range subject.Follow {
		if user == accountOrTag {
			createFile(basePath+subject.Name+"/", filename, resp)

		}
	}

	for _, tag := range subject.Hashtags {
		if tag == accountOrTag {
			createFile(basePath+subject.Name+"/", filename, resp)
		}
	}

	logrus.WithFields(logrus.Fields{
		"Subject":  subject,
		"Filename": filename}).Info("Downloading image...")
}

// noFallbackDownload is called if the fallback option is not enabled
// This gets the file and puts them in the Undetected and Unknown if applicable
// Otherwise the subject folder is used if the image has a known subject's face
func noFallbackDownload(subject string, filename string, basePath string, resp *http.Response) {
	if filename != "" {
		if subject == "Undetected" {
			createFile(basePath+"/NoFaces/", filename, resp)
		} else if subject == "Unknown" {
			createFile(basePath+"/UnknownFaces/", filename, resp)
		} else {
			createFile(basePath+subject+"/", filename, resp)
		}
	} else {
		basename := path.Base(resp.Request.URL.String())

		if subject == "Undetected" {
			createFile(basePath+"/NoFaces/", basename, resp)
		} else if subject == "Unknown" {
			createFile(basePath+"/UnknownFaces/", basename, resp)
		} else {
			createFile(basePath+subject+"/", basename, resp)
		}
	}

	logrus.WithFields(logrus.Fields{
		"Subject":  subject,
		"Filename": filename}).Info("Downloading image...")
}

// createFile creates the file using the response body in the path with the filename
// Thanks to https://stackoverflow.com/a/33845771
func createFile(pathname string, filename string, resp *http.Response) {
	// Create the file
	out, errCreate := os.Create(pathname + filename)
	if errCreate != nil {
		logrus.Error("Failed to create file.")
	}
	errHelp(errCreate)
	defer func() {
		errOutClose := out.Close()
		if errOutClose != nil {
			logrus.Error("Failed to defer close output.")
		}
		errHelp(errOutClose)
	}()

	defer func() {
		errRespClose := resp.Body.Close()
		if errRespClose != nil {
			logrus.Error("Failed to defer close response body.")
		}
		errHelp(errRespClose)
	}()

	// Writer the body to file
	_, errCopy := io.Copy(out, resp.Body)
	if errCopy != nil {
		logrus.Error("Failed to copy response to file.")
	}
	errHelp(errCopy)
}

// CreateConfigSamples creates the sample configuration files if actual ones don't exist yet
func CreateConfigSamples() {
	logrus.Debug("Creating configuration sample defaults...")

	if _, errStat := os.Stat("./config.toml"); os.IsNotExist(errStat) {
		WriteConfig()
	}

	if _, errStat := os.Stat("./follow.toml"); os.IsNotExist(errStat) {
		WriteFollow()
	}
}

// CreateDirectories creates folders for downloading images into, if not yet there
func CreateDirectories() {
	logrus.Debug("Trying to create image download folders...")
	fp := GetDownloadBasePath()

	// Create folders for downloading images into, if option is turned on
	if GetDownloadBool() {
		if _, errStat := os.Stat(fp); os.IsNotExist(errStat) {
			errMk := os.Mkdir(fp, 0777)
			errMkImgDir(errMk)
		}
		for _, person := range GetSubjects() {
			if _, errStat := os.Stat(fp + person); os.IsNotExist(errStat) {
				errMk := os.Mkdir(fp+person, 0777)
				errMkImgDir(errMk)
			}
		}

		// Create a folder for unknown faces, if option is turned on
		if GetUnknownBool() {
			if _, errStat := os.Stat(fp); os.IsNotExist(errStat) {
				errMk := os.Mkdir(fp, 0777)
				errMkImgDir(errMk)
			}
			errMk := os.Mkdir(fp+"/UnknownFaces", 0777)
			errMkImgDir(errMk)
		}

		// Create a folder for images without detected faces, if option is turned on
		if GetUndetectedBool() {
			if _, errStat := os.Stat(fp); os.IsNotExist(errStat) {
				errMk := os.Mkdir(fp, 0777)
				errMkImgDir(errMk)
			}
			errMk := os.Mkdir(fp+"/NoFaces", 0777)
			errMkImgDir(errMk)
		}
	}

	logrus.Debug("Created image download folders...")
}

// OpenDB opens the somina database and returns the database to be used
func OpenDB() *buntdb.DB {
	logrus.Debug("Opening Bunt database...")
	// Open the somina.db data file in your current directory.
	// It will be created if it doesn't exist.
	smDB, errDB := buntdb.Open("./sm.db")
	if errDB != nil {
		logrus.Fatal("Cannot open database!")
		errHelp(errDB)
	}

	return smDB
}

func errMkImgDir(errMk error) {
	if errMk != nil {
		logrus.Error("Failed to create image directory.")
	}
	errHelp(errMk)
}
