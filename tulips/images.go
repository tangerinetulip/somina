/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

import (
	"fmt"
	"image/jpeg"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"github.com/parnurzeal/gorequest"
	"github.com/sirupsen/logrus"
	"github.com/tidwall/buntdb"

	"github.com/tangerinetulip/goimagehash"
)

// ImageProcess gets the image from the URL, gets its hash and stores the hash and the *discordgo.Message in BoltDB
func ImageProcess(url string, message *discordgo.Message) {
	logrus.Debug("Starting to process image...")

	smDB := OpenDB()

	resp, _, errs := gorequest.New().Get(url).End()
	for _, errRequest := range errs {
		if errRequest != nil {
			logrus.Error("Failed to request URL.")
		}
		errHelp(errRequest)
	}

	image, errImage := jpeg.Decode(resp.Body)
	errHelp(errImage)

	hash, errHash := goimagehash.PerceptionHash(image)
	errHelp(errHash)

	logrus.WithFields(logrus.Fields{"Hash": hash.Hash}).Debug("Hashed image...")

	errDB := smDB.Update(func(tx *buntdb.Tx) error {
		_, _, errSet := tx.Set(fmt.Sprint(hash.Hash), message.ID, nil)
		if errSet != nil {
			logrus.Error("Failed to set values.")
		}
		return errSet
	})
	if errDB != nil {
		logrus.Fatal("Failed to update database!")
		errHelp(errDB)
	}
	defer func() {
		errClose := smDB.Close()
		if errClose != nil {
			logrus.Error("Failed to defer close database.")
		}
		errHelp(errClose)
	}()

	logrus.WithFields(logrus.Fields{"ID": message.ID}).Info("Storing message and hash...")
}

// GetHashMessage gets the URL of a new message, and returns the message ID as a string if the image is highly similar to a previous one
func GetHashMessage(url string, message *discordgo.Message) string {
	logrus.Debug("Checking database for image hash...")

	smDB := OpenDB()

	var returnValue string

	resp, _, errRequest := gorequest.New().Get(url).End()
	if !okReq(errRequest, resp) {
		return ""
	}

	image, errImage := jpeg.Decode(resp.Body)
	errHelp(errImage)

	hash, errHash := goimagehash.PerceptionHash(image)
	errHelp(errHash)

	errDB := smDB.View(func(tx *buntdb.Tx) error {

		_, errGet := tx.Get(fmt.Sprint(hash.Hash))
		if errGet == nil {
			logrus.Info("Found an exact image match in database...")
			returnValue = message.ID
			return nil
		} else if errGet != nil {
			logrus.Debug("No existing hash found.")
			errHelp(errGet)
		}

		errTX := tx.Ascend("", func(hashString, messageID string) bool {
			if message.ID == messageID {
				return false
			}
			hashUint, errParse := strconv.ParseUint(hashString, 10, 64)
			if errParse != nil {
				logrus.Error("Failed to parse hash into Uint.")
			}
			errHelp(errParse)
			useHash := goimagehash.NewImageHash(hashUint, GetHash())

			distance, err := hash.Distance(useHash)
			errHelp(err)

			if distance < 5 {
				logrus.Info("Found a similar image in database...")
				returnValue = messageID
			}
			return true
		})
		if errTX != nil {
			logrus.Fatal("Failed to iterate through database!")
		}
		return errTX
	})
	if errDB != nil {
		logrus.Fatal("Failed to view database!")
		errHelp(errDB)
	}
	defer func() {
		errClose := smDB.Close()
		if errClose != nil {
			logrus.Error("Failed to defer close database.")
		}
		errHelp(errClose)
	}()

	logrus.WithFields(logrus.Fields{"ID": returnValue}).Info("Getting hash and ID...")
	return returnValue
}
