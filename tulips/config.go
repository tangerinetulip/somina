/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/BurntSushi/toml"
	"github.com/sirupsen/logrus"

	"github.com/tangerinetulip/kairos-sdk-go"
)

var config Config
var galleries Galleries

// Read config.toml and follow.toml and pass them to structs
func init() {
	logrus.Debug("Reading configuration files for the first time...")

	_, errRead := toml.DecodeFile("./config.toml", &config)
	if errRead != nil {
		logrus.Fatal("Failed to read config.toml!")
	}
	errHelp(errRead)

	_, errRead = toml.DecodeFile("./follow.toml", &galleries)
	if errRead != nil {
		logrus.Fatal("Failed to read follow.toml!")
	}
	errHelp(errRead)
}

func reread() {
	logrus.Debug("Reading configuration files again...")

	_, errRead := toml.DecodeFile("./config.toml", &config)
	if errRead != nil {
		logrus.Fatal("Failed to read config.toml!")
	}
	errHelp(errRead)

	_, errRead = toml.DecodeFile("./follow.toml", &galleries)
	if errRead != nil {
		logrus.Fatal("Failed to read follow.toml!")
	}
	errHelp(errRead)
}

// EditLists adds the given list to an edited version of the current configuration
func EditLists(subjectName string, listID int64) {
	logrus.Debug("Adding a list to follow.toml...")

	buf := new(bytes.Buffer)

	subjectItem := Subject{}

	// Thanks to https://stackoverflow.com/a/13322410
	newFollow := newGallery()

	editFollow := galleries
	for galleryIndex, gallery := range editFollow.Gallery {
		for subjectIndex, subject := range gallery.Subjects {

			if subject.Name == subjectName {
				lists := editFollow.Gallery[galleryIndex].Subjects[subjectIndex].Lists

				newLists := append(lists, listID)
				subjectItem.Name = subjectName
				subjectItem.Channel = subject.Channel
				subjectItem.Follow = subject.Follow
				subjectItem.Hashtags = subject.Hashtags
				subjectItem.Lists = newLists

				newFollow.add(gallery.Gallery, gallery.Server, subjectName, subjectItem)
			} else if subject.Name != subjectName {
				subjectItem.Name = subject.Name
				subjectItem.Channel = subject.Channel
				subjectItem.Follow = subject.Follow
				subjectItem.Hashtags = subject.Hashtags
				subjectItem.Lists = subject.Lists

				newFollow.add(gallery.Gallery, gallery.Server, subject.Name, subjectItem)
			}
		}
	}

	if errEncode := toml.NewEncoder(buf).Encode(newFollow); errEncode != nil {
		logrus.Error("Failed to encode new list.")
		errHelp(errEncode)
	}
	errWrite := ioutil.WriteFile("./follow.toml", buf.Bytes(), 0644)
	if errWrite != nil {
		logrus.Error("Failed to write to follow.toml after editing.")
	}
	errHelp(errWrite)

	logrus.Debug("Added a list to follow.toml...")

	reread()
}

// WriteConfig writes the default config TOML files
func WriteConfig() {
	logrus.Debug("Writing default config.toml...")

	buf := new(bytes.Buffer)

	configTOML := &Config{}
	configTOML.Bot.PostUndetectedFaces = false
	configTOML.Bot.PostUnknownFaces = false
	configTOML.Bot.Prefix = "~"
	configTOML.Bot.DownloadBasePath = "./images"
	configTOML.Bot.DownloadImages = true
	configTOML.Bot.DownloadUnknown = false
	configTOML.Bot.DownloadUndetected = false
	configTOML.Bot.UseFallback = true
	configTOML.Bot.UseCleanup = false
	configTOML.Bot.LogLevel = "info"
	configTOML.Discord.Token = "your Discord token"
	configTOML.Kairos.AppID = "your Kairos app ID"
	configTOML.Kairos.APIKey = "your Kairos API key"
	configTOML.Twitter.ConsumerKey = "your Twitter consumer key"
	configTOML.Twitter.ConsumerSecret = "your Twitter consumer secret"
	configTOML.Twitter.AccessToken = "your Twitter access token"
	configTOML.Twitter.AccessTokenSecret = "your Twitter token secret"

	if errConfig := toml.NewEncoder(buf).Encode(configTOML); errConfig != nil {
		logrus.Error("Failed to encode sample config.toml.")
		errHelp(errConfig)
	}
	errWrite := ioutil.WriteFile("./config.sample_toml", buf.Bytes(), 0644)
	if errWrite != nil {
		logrus.Error("Failed to write sample config.toml.")
	}
	errHelp(errWrite)

	logrus.Debug("Wrote default config.toml...")
}

// WriteFollow writes the default follow TOML files
func WriteFollow() {
	logrus.Debug("Writing default follow.toml...")

	buf := new(bytes.Buffer)

	subject := Subject{}
	subject.Name = "Subject Name"
	subject.Channel = 0
	subject.Follow = []string{"@subject", "@twitter", "@accounts"}
	subject.Hashtags = []string{"#subject", "#twitter", "#hashtags"}
	subject.Lists = []int64{}

	// Thanks to https://stackoverflow.com/a/13322410
	defaultGallery := newGallery()
	defaultGallery.add("GalleryOne", 0, "First", subject)

	// Add a General subject for non-specific follows, hashtags and lists
	subject.Name = "General"
	defaultGallery.add("GalleryOne", 0, "General", subject)

	defaultGalleries := newGalleries()
	defaultGalleries.add("One", *defaultGallery)

	if errFollow := toml.NewEncoder(buf).Encode(defaultGalleries); errFollow != nil {
		logrus.Error("Failed to encode sample follow.toml.")
		errHelp(errFollow)
	}
	err := ioutil.WriteFile("./follow.sample_toml", buf.Bytes(), 0644)
	if err != nil {
		logrus.Error("Failed to write sample follow.toml.")
	}
	errHelp(err)

	logrus.Debug("Wrote default follow.toml...")
}

func newGalleries() *Galleries {
	return &Galleries{Gallery: make(map[string]Gallery)}
}

func (g *Galleries) add(gallery string, galleryStruct Gallery) {
	g.Gallery[gallery] = galleryStruct
}

func newGallery() *Gallery {
	return &Gallery{Subjects: make(map[string]Subject)}
}

func (g *Gallery) add(gallery string, server int64, subject string, subjectStruct Subject) {
	g.Gallery = gallery
	g.Server = server
	g.Subjects[subject] = subjectStruct
}

// GetBotPrefix returns the bot prefix for use in commands
func GetBotPrefix() string {
	logrus.Debug("Getting Discord bot prefix...")

	botPrefix := config.Bot.Prefix
	return botPrefix
}

// GetDownloadBasePath gets the configured base path for image downloads
func GetDownloadBasePath() string {
	logrus.Debug("Getting image download base path...")

	path := config.Bot.DownloadBasePath
	return path
}

// GetDownloadBool returns whether or not to download images
func GetDownloadBool() bool {
	logrus.Debug("Getting whether or not to download images...")

	download := config.Bot.DownloadImages
	return download
}

// GetUnknownBool returns whether or not to download images with unknown faces
func GetUnknownBool() bool {
	logrus.Debug("Getting whether or not to download unknown faces...")

	download := config.Bot.DownloadUnknown
	return download
}

// GetUndetectedBool returns whether or not to download images without detected faces
func GetUndetectedBool() bool {
	logrus.Debug("Getting whether or not to download images without faces...")

	download := config.Bot.DownloadUndetected
	return download
}

// GetCleanupBool returns whether or not the bot will delete duplicates
func GetCleanupBool() bool {
	logrus.Debug("Getting whether or not to clean messages up")

	cleanup := config.Bot.UseCleanup
	return cleanup
}

// SetLogLevel sets Logrus log level based on configured option.
func SetLogLevel() {
	logrus.Debug("Setting Logrus logging level...")

	switch config.Bot.LogLevel {
	case "panic":
		logrus.SetLevel(logrus.PanicLevel)
	case "fatal":
		logrus.SetLevel(logrus.FatalLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	default:
		logrus.SetLevel(logrus.InfoLevel)
	}
}

// GetSentryDSN returns the DSN used for Sentry.io error tracking
func GetSentryDSN() string {
	logrus.Debug("Getting Sentry DSN...")

	dsn := config.Sentry.DSN
	return dsn
}

// GetDiscordToken returns Discord bot token
func GetDiscordToken() string {
	logrus.Debug("Getting Discord Token...")

	discordAuth := config.Discord.Token
	return discordAuth
}

// GetDefaultChannel returns the default channel used for posting
func GetDefaultChannel(galleryGiven string) string {
	for _, gallery := range galleries.Gallery {
		if gallery.Gallery == galleryGiven {
			for _, subject := range gallery.Subjects {
				if strings.ToLower(subject.Name) == "general" {
					logrus.WithFields(
						logrus.Fields{
							"Channel": subject.Channel,
							"Gallery": gallery.Gallery}).Debug("Getting general channel for gallery...")

					defaultChannel := fmt.Sprint(subject.Channel)
					return defaultChannel
				}
			}
		}
	}
	return ""
}

// GetTalkChannels returns the channel where people talk, where deletion checks are not run
func GetTalkChannels() []int64 {
	logrus.Debug("Getting list of configured Talk channels...")

	var returnSlice []int64

	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			if subject.Name == "General" {
				returnSlice = append(returnSlice, subject.Channel)
			}
		}
	}

	return returnSlice
}

// SetKairosID sets the Kairos App ID for the library
func SetKairosID() {
	logrus.Debug("Setting Kairos app ID...")

	kairosAppID := config.Kairos.AppID
	kairos.SetAppID(kairosAppID)
}

// SetKairosKey sets the Kairos API key for the library
func SetKairosKey() {
	logrus.Debug("Setting Kairos API key...")

	kairosAPIKey := config.Kairos.APIKey
	kairos.SetAPIKey(kairosAPIKey)
}

// GetConsumerKey returns Twitter consumer key
func GetConsumerKey() string {
	logrus.Debug("Getting Twitter consumer key...")

	twitterConsumerKey := config.Twitter.ConsumerKey
	return twitterConsumerKey
}

// GetConsumerSecret returns Twitter consumer secret
func GetConsumerSecret() string {
	logrus.Debug("Getting Twitter consumer secret...")

	twitterConsumerSecret := config.Twitter.ConsumerSecret
	return twitterConsumerSecret
}

// GetAccessToken returns Twitter access token
func GetAccessToken() string {
	logrus.Debug("Getting Twitter access token...")

	twitterAccessToken := config.Twitter.AccessToken
	return twitterAccessToken
}

// GetAccessSecret returns Twitter access token secret
func GetAccessSecret() string {
	logrus.Debug("Getting Twitter access token secret...")

	twitterAccessTokenSecret := config.Twitter.AccessTokenSecret
	return twitterAccessTokenSecret
}

// IsKairosSubjectChannel checks if given string is a subject in follow.toml and returns truth value along with the subject's channel
func IsKairosSubjectChannel(mightBeSubject string) (bool, int64) {
	var isSubject bool
	var channel int64

	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			if subject.Name == mightBeSubject {
				isSubject = true
				channel = subject.Channel
			}
		}
	}

	logrus.WithFields(logrus.Fields{
		"Subject": mightBeSubject,
		"Is":      isSubject,
		"Channel": channel}).Info("Checking if Subject...")
	return isSubject, channel
}

// IsKairosGallery checks if given string is a gallery in follow.toml and returns truth value
func IsKairosGallery(mightBeGallery string) bool {
	var isGallery bool

	for _, gallery := range galleries.Gallery {
		if mightBeGallery == gallery.Gallery {
			isGallery = true
		}
	}

	logrus.WithFields(logrus.Fields{
		"Gallery":    mightBeGallery,
		"Is Gallery": isGallery}).Info("Checking if gallery...")
	return isGallery
}

// GetTwitterFollows returns all Twitter accounts in follow.toml as a string with @ removed
func GetTwitterFollows() string {
	accountsList := ""

	for _, gallery := range galleries.Gallery {
		for _, person := range gallery.Subjects {
			for _, account := range person.Follow {
				if true {
					accountsList += strings.TrimPrefix(account, "@") + ","
				}
			}
		}
	}

	logrus.Info("Getting all Twitter accounts to follow...")
	accountsList = strings.TrimSuffix(accountsList, ",")
	return accountsList
}

// GetTwitterTags returns all Twitter hashtags in follow.toml as a string with # removed
func GetTwitterTags() string {
	hashtagList := ""

	for _, gallery := range galleries.Gallery {
		for _, person := range gallery.Subjects {
			for _, tag := range person.Hashtags {
				if true {
					hashtagList += strings.TrimPrefix(tag, "#") + ","
				}
			}
		}
	}

	logrus.Info("Getting all Twitter hashtags to check...")
	hashtagList = strings.TrimSuffix(hashtagList, ",")
	return hashtagList
}

// GetTwitterLists returns all Twitter lists in follow.toml as an int64 slice
func GetTwitterLists() []int64 {
	twitterLists := []int64{}

	for _, gallery := range galleries.Gallery {
		for _, person := range gallery.Subjects {
			twitterLists = append(twitterLists, person.Lists...)
		}
	}

	logrus.Info("Getting all Twitter lists to follow...")
	return twitterLists
}

// GetSubjects returns a slice of strings containing all names of configured subjects
func GetSubjects() []string {
	subjects := []string{}

	for _, gallery := range galleries.Gallery {
		for _, person := range gallery.Subjects {
			subjects = append(subjects, person.Name)
		}
	}

	logrus.Info("Getting all configured subjects...")
	return subjects
}
