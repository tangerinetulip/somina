/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

// Thanks to https://github.com/SubliminalHQ/karen/blob/master/src/cache/session.go
import (
	"sync"

	"github.com/bwmarrin/discordgo"
	"github.com/getsentry/raven-go"
	"github.com/sirupsen/logrus"

	"github.com/tangerinetulip/anaconda"
	"github.com/tangerinetulip/goimagehash"
)

var (
	discordSession *discordgo.Session
	twitterSession *anaconda.TwitterApi
	imageHash      goimagehash.HashKind
	sessionMutex   sync.RWMutex
)

// SetDiscordSession sets the Discord session
func SetDiscordSession(s *discordgo.Session) {
	sessionMutex.Lock()
	discordSession = s
	sessionMutex.Unlock()
}

// GetDiscordSession returns recorded Discord session
func GetDiscordSession() *discordgo.Session {
	sessionMutex.RLock()
	defer sessionMutex.RUnlock()

	if discordSession == nil {
		logrus.Fatal("Tried to get Discord session without setting it.")
	}

	return discordSession
}

// SetTwitterClient sets the Twitter client to use
func SetTwitterClient(s *anaconda.TwitterApi) {
	sessionMutex.Lock()
	twitterSession = s
	sessionMutex.Unlock()
}

// GetTwitterClient returns recorded Twitter client
func GetTwitterClient() *anaconda.TwitterApi {
	sessionMutex.RLock()
	defer sessionMutex.RUnlock()

	if twitterSession == nil {
		logrus.Fatal("Tried to get Twitter client without setting it.")
	}

	return twitterSession
}

// GetHash returns PHash
func GetHash() goimagehash.HashKind {
	imageHash = goimagehash.PHash

	return imageHash
}

func errHelp(err error) {
	if err != nil {
		raven.CaptureError(err, nil)
		return
	}
}
