/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

import (
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/parnurzeal/gorequest"
	"github.com/sirupsen/logrus"

	"github.com/tangerinetulip/anaconda"
)

var (
	recognizeChan = make(chan Recognize, 100)
)

// CheckAllTwitter starts the process for checking
// configured hashtags, lists and accounts
func CheckAllTwitter() {
	var usersInList string

	twit := GetTwitterClient()

	accountsList := GetTwitterFollows()
	hashtagList := GetTwitterTags()
	twitterList := GetTwitterLists()

	for _, listID := range twitterList {
		users, errList := twit.GetListMembers(listID, nil)
		if errList != nil {
			logrus.Error("Failed to list members of Twitter list.")
		}
		errHelp(errList)

		for _, listAccount := range users {
			usersInList += listAccount.IdStr + ","
		}
	}

	v := url.Values{}
	v.Set("user_id", accountsList+usersInList)
	v.Set("track", hashtagList)
	v.Set("with", "user")

	userStream := twit.UserStream(v)

	func(stream *anaconda.Stream) {
		logrus.Info("Twitter goroutines starting...")
		for {
			s := <-stream.C
			switch tweet := s.(type) {
			case anaconda.Tweet:
				go CheckTwitterAccounts(tweet)
				go CheckTwitterLists(tweet)
				go CheckTwitterTags(tweet)
			default:
				// pass
			}
		}
	}(userStream)
}

// CheckTwitterAccounts gets user streams and processes images into Kairos then posts to respective channels
func CheckTwitterAccounts(tweet anaconda.Tweet) {
	if getSubjectFromAccount(tweet.User.ScreenName) == "" {
		logrus.Debug("Account not found in config, ignoring...")
		return
	}

	for _, foundURL := range tweet.Entities.Urls {
		logrus.Info("Found Tweet from " + tweet.User.ScreenName)
		resp, _, errs := gorequest.New().Get(foundURL.Expanded_url).End()
		if !okReq(errs, resp) {
			return
		}

		recognize := Recognize{
			URL:          resp.Request.URL.String(),
			AccountOrTag: tweet.User.ScreenName,
			SubjectName:  getSubjectFromAccount(tweet.User.ScreenName)}
		recognizeChan <- recognize

		KairosRecognize(recognizeChan)
	}
	for _, mediaURL := range tweet.ExtendedEntities.Media {
		recognize := Recognize{
			URL:          mediaURL.Media_url_https,
			AccountOrTag: tweet.User.ScreenName,
			SubjectName:  getSubjectFromAccount(tweet.User.ScreenName)}
		recognizeChan <- recognize

		KairosRecognize(recognizeChan)
	}
}

// CheckTwitterTags gets hashtag streams and processes images into Kairos then posts to respective channels
func CheckTwitterTags(tweet anaconda.Tweet) {
	hashtags := GetTwitterTags()

	var usedTag string
	// This bit is supposed to check if the hashtag is in the Tweet's text
	for _, hashtag := range strings.Split(hashtags, ",") {
		match, errMatch := regexp.MatchString(hashtag, tweet.Text)
		if errMatch != nil {
			logrus.Error("Failed to match strings!")
		}
		errHelp(errMatch)

		if match {
			usedTag = hashtag
		}
	}

	if usedTag == "" {
		logrus.Debug("Hashtag not in config, ignoring...")
	}

	for _, foundURL := range tweet.Entities.Urls {
		logrus.Info("Found Tweet from " + tweet.User.ScreenName)
		resp, _, errs := gorequest.New().Get(foundURL.Expanded_url).End()
		if !okReq(errs, resp) {
			return
		}

		recognize := Recognize{
			URL:          resp.Request.URL.String(),
			AccountOrTag: usedTag,
			SubjectName:  getSubjectFromHashtag(usedTag)}
		recognizeChan <- recognize

		KairosRecognize(recognizeChan)
	}
	for _, mediaURL := range tweet.ExtendedEntities.Media {
		recognize := Recognize{
			URL:          mediaURL.Media_url_https,
			AccountOrTag: usedTag,
			SubjectName:  getSubjectFromHashtag(usedTag)}
		recognizeChan <- recognize

		KairosRecognize(recognizeChan)
	}
}

// CheckTwitterLists gets all lists and their associated accounts and processes those tweets for Kairos and Discord
func CheckTwitterLists(tweet anaconda.Tweet) {
	subjectName := getSubjectFromList(tweet.User.ScreenName)

	if subjectName == "" {
		logrus.Debug("User not in any list in config, ignoring...")
		return
	}

	for _, foundURL := range tweet.Entities.Urls {
		logrus.Info("Found Tweet from " + tweet.User.ScreenName)
		resp, _, errs := gorequest.New().Get(foundURL.Expanded_url).End()
		if !okReq(errs, resp) {
			return
		}

		recognize := Recognize{
			URL:          resp.Request.URL.String(),
			AccountOrTag: tweet.User.ScreenName,
			SubjectName:  subjectName}
		recognizeChan <- recognize

		KairosRecognize(recognizeChan)
	}
	for _, mediaURL := range tweet.ExtendedEntities.Media {
		recognize := Recognize{
			URL:          mediaURL.Media_url_https,
			AccountOrTag: tweet.User.ScreenName,
			SubjectName:  subjectName}
		recognizeChan <- recognize

		KairosRecognize(recognizeChan)
	}
}

func getSubjectFromAccount(screenName string) string {
	var returnString string

	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			for _, account := range subject.Follow {
				trimmedAccount := strings.Trim(account, "@")

				if screenName == trimmedAccount {
					logrus.WithFields(logrus.Fields{
						"Given User":  screenName,
						"Config User": trimmedAccount}).Debug("Found user in config...")
					returnString = subject.Name
				}
			}
		}
	}

	if !(returnString == "") {
		logrus.WithFields(logrus.Fields{"Subject": returnString}).Debug("Returning subject from account...")
	} else {
		logrus.Debug("Subject not in follows...")
	}
	return returnString
}

func getSubjectFromHashtag(hashtag string) string {
	var returnString string

	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			for _, account := range subject.Hashtags {
				trimmedHashtag := strings.Trim(account, "#")

				if hashtag == trimmedHashtag {
					logrus.WithFields(logrus.Fields{
						"Given Hashtag":  hashtag,
						"Config Hashtag": trimmedHashtag}).Debug("Found hashtag in config...")
					returnString = subject.Name
				}
			}
		}
	}

	if !(returnString == "") {
		logrus.WithFields(logrus.Fields{"Subject": returnString}).Debug("Returning subject from hashtag...")
	} else {
		logrus.Debug("Subject not in hashtags...")
	}
	return returnString
}

func getSubjectFromList(userScreenName string) string {
	twit := GetTwitterClient()
	var returnString string

	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			for _, list := range subject.Lists {
				users, errList := twit.GetListMembers(list, nil)
				errGetListMem(errList)

				for _, userAccount := range users {
					for _, account := range subject.Follow {
						trimmedAccount := strings.Trim(account, "@")

						if userAccount.ScreenName == trimmedAccount {
							logrus.WithFields(logrus.Fields{
								"Given User":  userScreenName,
								"Config User": trimmedAccount}).Debug("Found user in config...")
							returnString = subject.Name
						}
					}
				}
			}
		}
	}

	if !(returnString == "") {
		logrus.WithFields(logrus.Fields{"Subject": returnString}).Debug("Returning subject from list...")
	} else {
		logrus.Debug("Subject not in lists...")
	}
	return returnString
}

func okReq(errs []error, resp *http.Response) bool {
	for _, errRequest := range errs {
		errHelp(errRequest)
		if errRequest != nil {
			logrus.Error("Failed to request a given URL.")
			return false
		}
	}

	if resp.StatusCode != 200 {
		logrus.Error("Given URL gave a non-OK response.")
		return false
	}

	return true
}

func errGetListMem(errList error) {
	if errList != nil {
		logrus.Error("Failed to list members of a Twitter list.")
	}
	errHelp(errList)
}
