/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"

	"github.com/tangerinetulip/kairos-sdk-go"
)

// KairosEnroll enrolls a photo as a subject reference
func KairosEnroll(discord *discordgo.Session, message *discordgo.Message, subject string, url string) {
	logrus.Debug("Trying to enroll subject to Kairos...")

	feedback := kairos.EnrollURLOr64(url, subject, getGalleryOfSubject(subject))

	if len(feedback.Images) > 0 {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, feedback.Images[0].Transaction.Status)
		errSendMessage(errSend)

		logrus.WithFields(logrus.Fields{
			"Subject": subject,
			"Status":  "Success"}).Info("Enrolling Subject...")
	} else {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "failure")
		errSendMessage(errSend)

		logrus.WithFields(logrus.Fields{
			"Subject": subject,
			"Status":  "Failure"}).Info("Enrolling Subject...")
	}
}

// KairosRemoveSubject removes the given subject in the gallery
func KairosRemoveSubject(message *discordgo.Message, subject string) {
	logrus.Debug("Trying to remove subject from Kairos...")

	discord := GetDiscordSession()

	feedback := kairos.RemoveSubject(subject, getGalleryOfSubject(subject))

	if len(feedback.Message) > 0 {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, feedback.Message)
		errSendMessage(errSend)

		logrus.WithFields(logrus.Fields{
			"Subject": subject,
			"Status":  "Success"}).Info("Removing Subject...")
	} else {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "failure")
		errSendMessage(errSend)

		logrus.WithFields(logrus.Fields{
			"Subject": subject,
			"Status":  "Failure"}).Info("Removing Subject...")
	}
}

// KairosView lists all subjects enrolled in the gallery
func KairosView(message *discordgo.Message, gallery string) {
	logrus.Debug("Trying to view gallery in Kairos...")

	discord := GetDiscordSession()

	feedback := kairos.ViewGallery(gallery)

	if len(feedback.SubjectIds) > 0 {
		subjectsMessage := ""
		for _, subject := range feedback.SubjectIds {
			subjectsMessage += subject + ", "
		}
		_, errSend := discord.ChannelMessageSend(message.ChannelID, subjectsMessage)
		errSendMessage(errSend)

		logrus.WithFields(logrus.Fields{
			"Gallery": gallery,
			"Status":  "Success"}).Info("Viewing...")
	} else {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "failure")
		errSendMessage(errSend)

		logrus.WithFields(logrus.Fields{
			"Gallery": gallery,
			"Status":  "Failure"}).Info("Viewing...")
	}
}

// KairosRecognize gets the Twitter URL and sends it to Kairos for recognition and posts it to respective Discord channel
func KairosRecognize(recognizeChan chan Recognize) {
	for recognize := range recognizeChan {
		recognize := recognize
		logrus.Debug("Trying to recognize an image...")

		url := recognize.URL
		accountOrTag := recognize.AccountOrTag
		subject := recognize.SubjectName

		feedback := kairos.RecognizeURLOr64(url, getGalleryOfAccTag(accountOrTag))
		logrus.WithFields(logrus.Fields{"Subject": subject}).Info("Recognizing...")

		if len(feedback.Errors) > 0 {
			logrus.WithFields(logrus.Fields{"Error": feedback.Errors[0].Message}).Debug("Error!")
			undetected(url, accountOrTag, subject, feedback)
		} else if feedback.Status != "failure" {
			detected(url, accountOrTag, feedback)
		} else {
			logrus.Warn("Failed to recognize the image.")
		}
	}
}

func detected(url string, accountOrTag string, feedback kairos.Feedback) {
	discord := GetDiscordSession()
	postedYet := false

	// For each transaction, check if received subject is known and then post accordingly
	// Also ignore the image if it has been already posted, before processing
	for _, transaction := range feedback.Images {
		transactionSubject := transaction.Transaction.SubjectID

		var isInConfig bool
		var channel int64

		if transactionSubject == "" || transactionSubject == " " {
			logrus.Warn("No transaction subject...")
			return
		}

		isInConfig, channel = IsKairosSubjectChannel(transactionSubject)

		if postedYet {
			return
		}

		logrus.WithFields(logrus.Fields{
			"Subject": transactionSubject,
			"In file": isInConfig}).Info("Kairos Transaction...")

		if isInConfig {
			if config.Bot.DownloadImages {
				DownloadImage(url, accountOrTag, transactionSubject)
			}
			_, errSend := discord.ChannelMessageSend(fmt.Sprint(channel), url)
			errSendMessage(errSend)

			logrus.WithFields(logrus.Fields{
				"Subject": transactionSubject,
				"Channel": channel,
				"Status":  "Recognized"}).Info("Posted...")

			postedYet = true
		} else {
			if config.Bot.DownloadUnknown {
				DownloadImage(url, accountOrTag, "Unknown")
			}
			if config.Bot.PostUnknownFaces {
				if config.Bot.UseFallback {
					useFallback(url, accountOrTag)
				} else {
					_, errSend := discord.ChannelMessageSend(GetDefaultChannel(getGalleryOfAccTag(accountOrTag)), url)
					errSendMessage(errSend)

					logrus.WithFields(logrus.Fields{
						"Subject": transactionSubject,
						"Channel": GetDefaultChannel(getGalleryOfAccTag(accountOrTag)),
						"Status":  "Unknown"}).Info("Posted...")
				}
				postedYet = true
			}
		}
	}
}

// undetected handles sending and downloading of images without detected faces
func undetected(url string, accountOrTag string, subject string, feedback kairos.Feedback) {
	logrus.Debug("Handling image with no detected faces...")

	discord := GetDiscordSession()

	// Post images with no detected faces in fallback channel if option is enabled, otherwise post in general channel
	if config.Bot.PostUndetectedFaces {
		if config.Bot.UseFallback {
			useFallback(url, accountOrTag)
		} else {
			defaultChannel := GetDefaultChannel(getGalleryOfAccTag(accountOrTag))
			_, errSend := discord.ChannelMessageSend(defaultChannel, feedback.Errors[0].Message+"/n"+url)
			errSendMessage(errSend)

			logrus.WithFields(logrus.Fields{
				"Subject": subject,
				"Channel": defaultChannel,
				"Status":  "Undetected"}).Info("Posted...")
		}
	}
	if config.Bot.DownloadUndetected {
		DownloadImage(url, accountOrTag, "Undetected")
	}
}

// useFallback handles sending images to fallback channels if the option is enabled
func useFallback(url string, accountOrTag string) {
	logrus.WithFields(logrus.Fields{
		"Account or Hashtag": accountOrTag}).Debug("Using fallback for image...")

	discord := GetDiscordSession()

	// If accountOrTag matches accounts in lists or hashtags in follow.toml
	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			twit := GetTwitterClient()
			channelStr := fmt.Sprint(subject.Channel)

			// This is specifically for handling list-given accounts because normal configured accounts
			// are listed by their names
			for _, list := range subject.Lists {
				users, err := twit.GetListMembers(list, nil)
				errHelp(err)

				for _, user := range users {
					if user.IdStr == accountOrTag {
						_, errSend := discord.ChannelMessageSend(channelStr, url)
						errSendMessage(errSend)

						logrus.WithFields(logrus.Fields{
							"Subject": subject,
							"Channel": subject.Channel,
							"Status":  "Undetected/Fallback"}).Info("Posted...")
					}
				}
			}

			// This is now for handling normal configured accounts
			for _, user := range subject.Follow {
				if user == accountOrTag {
					_, errSend := discord.ChannelMessageSend(channelStr, url)
					errSendMessage(errSend)

					logrus.WithFields(logrus.Fields{
						"Subject": subject,
						"Channel": subject.Channel,
						"Status":  "Undetected/Fallback"}).Info("Posted...")
				}
			}

			// This is for handling hashtags
			for _, tag := range subject.Hashtags {
				if tag == accountOrTag {
					_, errSend := discord.ChannelMessageSend(channelStr, url)
					errSendMessage(errSend)

					logrus.WithFields(logrus.Fields{
						"Subject": subject,
						"Channel": subject.Channel,
						"Status":  "Undetected/Fallback"}).Info("Posted...")
				}
			}
		}
	}
}

// getGallery returns the gallery of the given account or hashtag
func getGalleryOfAccTag(accountTagLookup string) string {
	logrus.WithFields(logrus.Fields{"Account or Tag": accountTagLookup}).Debug("Getting gallery of account or tag...")
	twit := GetTwitterClient()

	var returnGallery string
	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			// Go through Subject accounts to see if given account is there
			for _, account := range subject.Follow {
				if accountTagLookup == strings.TrimPrefix(account, "@") {
					returnGallery = gallery.Gallery
				}
			}

			// Go through Subject lists to see if given account is in a list there
			for _, list := range subject.Lists {
				users, errList := twit.GetListMembers(list, nil)
				errGetListMem(errList)

				for _, userAccount := range users {
					if accountTagLookup == userAccount.ScreenName {
						returnGallery = gallery.Gallery
					}
				}
			}

			// Go through Subject hashtags if given hashtag is there
			for _, tag := range subject.Hashtags {
				if accountTagLookup == strings.TrimPrefix(tag, "#") {
					returnGallery = gallery.Gallery
				}
			}
		}
	}

	logrus.WithFields(logrus.Fields{
		"Account or Tag": accountTagLookup,
		"Gallery":        returnGallery}).Debug("Returning gallery of account or tag...")
	return returnGallery
}

// getGallery returns the gallery of the given subject
func getGalleryOfSubject(subjectLookup string) string {
	logrus.WithFields(logrus.Fields{"Subject": subjectLookup}).Debug("Getting gallery of subject...")

	var returnGallery string
	for _, gallery := range galleries.Gallery {
		for _, subject := range gallery.Subjects {
			if subjectLookup == subject.Name {
				returnGallery = gallery.Gallery
			}
		}
	}

	logrus.WithFields(logrus.Fields{
		"Subject": subjectLookup,
		"Gallery": returnGallery}).Debug("Returning gallery of subject...")
	return returnGallery
}
