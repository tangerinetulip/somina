/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

import (
	"fmt"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/parnurzeal/gorequest"
	"github.com/sirupsen/logrus"
)

var (
	messageChan = make(chan *discordgo.MessageCreate, 100)
)

// HandleMessages handles every message that is sent to the server for processing commands
func HandleMessages(discord *discordgo.Session, message *discordgo.MessageCreate) {

	messageChan <- message
	go goProcessClean(messageChan)

	// Ignore all messages created by bots and global mentions
	adminID, errParse := strconv.ParseInt(message.Author.ID, 10, 64)
	if errParse != nil {
		logrus.Error("Failed to parse message author ID.")
	}
	errHelp(errParse)
	if message.Author.Bot || message.MentionEveryone || adminID != config.Discord.DefaultAdmin {
		return
	}

	// Find command and arguments
	// Thanks to https://github.com/darkliquid/D0g/blob/master/command_handler.go
	if strings.HasPrefix(message.Content, GetBotPrefix()) {
		commandParts := strings.Split(strings.TrimPrefix(message.Content, GetBotPrefix()), " ")
		command := strings.ToLower(commandParts[0])

		doCommand(command, discord, message.Message)
	}
}

func goProcessClean(messageCreateChan chan *discordgo.MessageCreate) {

	message := <-messageCreateChan

	// Run cleanup if activated and before processing images
	// to prevent auto-deletion of every new one
	if GetCleanupBool() {
		cleanup(message.Message)
	}

	// Almost same construction as the cleanup method
	// but this is for processing the images into the database
	if !strings.HasPrefix(message.Content, GetBotPrefix()) {
		for _, channel := range GetTalkChannels() {
			if !(message.ChannelID == fmt.Sprint(channel)) {
				process(message.Message)
			}
		}
	}
}

// cleanup processes messages and images linked for PHash comparison and cleanup
func cleanup(message *discordgo.Message) {
	discord := GetDiscordSession()

	// If no prefix and not in talk channel, do cleanup
	if !strings.HasPrefix(message.Content, GetBotPrefix()) {
		for _, channel := range GetTalkChannels() {
			if message.ChannelID == fmt.Sprint(channel) {
				return
			}
		}

		args := strings.Split(message.Content, " ")

		for _, arg := range args {
			// Only process if it's a valid URL and with a valid extension
			if isValidURL(arg) {
				fileExt := regexp.MustCompile(`[^"]+\.[^\\"]{3,4}`)
				argBase := path.Base(arg)
				if fileExt.FindString(argBase) != "" {
					messageID := GetHashMessage(arg, message)

					if messageID == "" || messageID == " " {
						return
					}

					errDelete := discord.ChannelMessageDelete(message.ChannelID, messageID)
					if errDelete != nil {
						logrus.Error("Failed to delete Discord message.")
					}
					errHelp(errDelete)

					logrus.WithFields(logrus.Fields{"ID": messageID}).Info("Deleted " + arg)
				}
			}
		}
	}
}

// process puts the linked image and its hash into the DB along with the message ID
func process(message *discordgo.Message) {
	args := strings.Split(message.Content, " ")

	for _, arg := range args {
		if isValidURL(arg) {
			fileExt := regexp.MustCompile(`[^"]+\.[^\\"]{3,4}`)
			argBase := path.Base(arg)

			existsInDB := GetHashMessage(arg, message)
			if existsInDB == message.ID {
				return
			}

			if fileExt.FindString(argBase) != "" {
				ImageProcess(arg, message)
			}
		}
	}
}

// doCommand handles Discord commands
func doCommand(command string, discord *discordgo.Session, message *discordgo.Message) {

	switch command {
	case "ping":
		ping(discord, message)
	case "help":
		help(discord, message)
	case "kairos":
		kairosCommands(discord, message)
	case "twitter":
		twitterCommands(discord, message)
	}
}

// ping measures ping between the computer running somina and Discord servers
func ping(discord *discordgo.Session, message *discordgo.Message) {

	// Thanks to https://github.com/SubliminalHQ/karen/blob/master/src/modules/ping.go
	start := time.Now()

	m, errSend := discord.ChannelMessageSend(message.ChannelID, ":ping_pong: Pong!")
	errSendMessage(errSend)

	end := time.Now()
	_, errEdit := discord.ChannelMessageEdit(
		message.ChannelID,
		m.ID,
		m.Content+" ("+strconv.Itoa(int(end.Sub(start)/time.Millisecond)/2)+"ms)",
	)
	if errEdit != nil {
		logrus.Error("Failed to edit Discord message.")
	}
	errHelp(errEdit)
}

// help simply shows the help message
func help(discord *discordgo.Session, message *discordgo.Message) {
	_, errSend := discord.ChannelMessageSend(message.ChannelID, "Please check <https://github.com/tangerinetulip/somina/wiki>.")
	errSendMessage(errSend)
}

// kairosCommands handles all commands related to Kairos
func kairosCommands(discord *discordgo.Session, message *discordgo.Message) {
	// Go ask Seklfreak@GitHub about this because I don't know what this is
	stopIt := false

	_, errSend := discord.ChannelMessageSend(message.ChannelID, "Commands:\nenroll, remove, view, exit")
	errSendMessage(errSend)
	closeHandler := discord.AddHandler(func(session *discordgo.Session, handlerMessage *discordgo.MessageCreate) {
		if stopIt {
			return
		}

		if handlerMessage.Author.ID != message.Author.ID {
			return
		}

		if !strings.HasPrefix(message.Content, GetBotPrefix()) {
			return
		}

		messageContentTrimmed := strings.TrimPrefix(handlerMessage.Content, GetBotPrefix())

		if handlerMessage.Author.ID == message.Author.ID {
			args := strings.Split(messageContentTrimmed, " ")

			passedArgs := args[1:]
			switch strings.ToLower(args[0]) {
			case "enroll":
				enroll(passedArgs, message)
			case "remove":
				remove(passedArgs, message)
			case "view":
				view(passedArgs, message)
			case "exit":
				_, errSend = session.ChannelMessageSend(message.ChannelID, "Exiting.")
				errSendMessage(errSend)

				stopIt = true
				return
			}
		}
	})
	time.Sleep(5 * time.Minute)
	_, errSend = discord.ChannelMessageSend(message.ChannelID, "Command window timeout.")
	errSendMessage(errSend)
	closeHandler()
}

// enroll parses the command and its arguments to process for Kairos subject enrollment
func enroll(args []string, message *discordgo.Message) {
	discord := GetDiscordSession()

	if len(args) < 2 {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check arguments.")
		errSendMessage(errSend)
		return
	}
	subject := args[0]
	URL := args[1]
	isSubject, _ := IsKairosSubjectChannel(subject)
	isURL := isValidURL(URL)
	if isSubject && isURL {
		KairosEnroll(discord, message, subject, URL)
	} else {
		if !isURL {
			_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check URL.")
			errSendMessage(errSend)
		} else if !isSubject {
			_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check subject.")
			errSendMessage(errSend)
		}
		return
	}
}

// remove parses the command and its arguments to process for removing a Kairos subject
func remove(args []string, message *discordgo.Message) {
	discord := GetDiscordSession()

	if len(args) < 1 {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check arguments.")
		errSendMessage(errSend)
		return
	}
	subject := args[0]
	isSubject, _ := IsKairosSubjectChannel(subject)
	if isSubject {
		KairosRemoveSubject(message, subject)
	} else {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check subject.")
		errSendMessage(errSend)
	}
}

// view gets the gallery name and lists all subjects enrolled within Kairos in that gallery
func view(args []string, message *discordgo.Message) {
	discord := GetDiscordSession()

	if len(args) < 1 {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check arguments.")
		errSendMessage(errSend)
		return
	}

	gallery := args[0]
	isGallery := IsKairosGallery(gallery)
	if isGallery {
		KairosView(message, gallery)
	} else {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check gallery name.")
		errSendMessage(errSend)
		return
	}
}

// twitter handles all commands related to Twitter
func twitterCommands(discord *discordgo.Session, message *discordgo.Message) {
	// Go ask Seklfreak@GitHub about this because I don't know what this is
	stopIt := false

	_, errSend := discord.ChannelMessageSend(message.ChannelID, "Commands:\nlistsby, addlist, exit")
	errSendMessage(errSend)
	errHelp(errSend)
	closeHandler := discord.AddHandler(func(discordSession *discordgo.Session, handlerMessage *discordgo.MessageCreate) {
		if stopIt {
			return
		}

		if handlerMessage.Author.ID != message.Author.ID {
			return
		}

		if !strings.HasPrefix(message.Content, GetBotPrefix()) {
			return
		}

		messageContentTrimmed := strings.TrimPrefix(handlerMessage.Content, GetBotPrefix())

		if handlerMessage.Author.ID == message.Author.ID {
			args := strings.Split(messageContentTrimmed, " ")

			passedArgs := args[1:]
			switch strings.ToLower(args[0]) {
			case "listsby":
				listsBy(passedArgs, message)
			case "addlist":
				addList(passedArgs, message)
			case "exit":
				_, errSend = discordSession.ChannelMessageSend(message.ChannelID, "Exiting.")
				errSendMessage(errSend)

				stopIt = true
				return
			}
		}
	})
	time.Sleep(5 * time.Minute)
	_, errSend = discord.ChannelMessageSend(message.ChannelID, "Command window timeout.")
	errSendMessage(errSend)
	closeHandler()
}

// listsBy lists all lists by the given user
func listsBy(args []string, message *discordgo.Message) {
	discord := GetDiscordSession()

	if len(args) < 1 {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check arguments.")
		errSendMessage(errSend)

		return
	}

	twitterClient := GetTwitterClient()
	listAccount, errSearch := twitterClient.GetUserSearch(args[0], nil)
	if errSearch != nil {
		logrus.Error("Failed to search for a given user.")
	}
	errHelp(errSearch)

	listsString := ""
	for _, userAccount := range listAccount {
		lists, errForList := twitterClient.GetListsOwnedBy(userAccount.Id, nil)
		if errForList != nil {
			logrus.Error("Failed to search for lists by a given user.")
		}
		errHelp(errForList)
		for _, list := range lists {
			listsString += list.Name + ": " + fmt.Sprint(list.Id) + "\n"
		}
	}
	listsString = listsString + "\n" + "There you go."
	_, errSend := discord.ChannelMessageSend(message.ChannelID, listsString)
	errSendMessage(errSend)
}

// addList adds the list ID to follow.toml and to the appropriate subject
func addList(args []string, message *discordgo.Message) {
	discord := GetDiscordSession()

	if len(args) < 2 {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check arguments.")
		errSendMessage(errSend)

		return
	}

	isSubject, _ := IsKairosSubjectChannel(args[0])
	if !isSubject {
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check subject name.")
		errSendMessage(errSend)

		return
	}

	intID, errParse := strconv.ParseInt(args[1], 10, 64)
	if errParse != nil {
		logrus.Error("Failed to parse given list ID into an integer.")
		errHelp(errParse)
		_, errSend := discord.ChannelMessageSend(message.ChannelID, "Check list ID.")
		errSendMessage(errSend)

		return
	}

	EditLists(args[0], intID)
	_, errSend := discord.ChannelMessageSend(message.ChannelID, "Added new list to "+args[0]+".")
	errSendMessage(errSend)
}

// isValidURL tests if the given string is an URL
func isValidURL(toTest string) bool {
	var finalError error

	_, _, errs := gorequest.New().Get(toTest).End()
	for _, errRequest := range errs {
		logrus.Error("Failed to request a given URL.")
		errHelp(errRequest)
		finalError = errRequest
	}

	// Returns true if error is nil
	return finalError == nil
}

func errSendMessage(errSend error) {
	if errSend != nil {
		logrus.Error("Failed to send Discord message.")
	}
	errHelp(errSend)
}
