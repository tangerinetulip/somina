/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package tulips

import (
	"github.com/bwmarrin/discordgo"
	"github.com/tangerinetulip/goimagehash"
)

// Config is config.toml as a struct
type Config struct {
	Bot struct {
		PostUndetectedFaces bool
		PostUnknownFaces    bool
		Prefix              string
		DownloadBasePath    string
		DownloadImages      bool
		DownloadUnknown     bool
		DownloadUndetected  bool
		UseFallback         bool
		UseCleanup          bool
		LogLevel            string
	}
	Sentry struct {
		DSN string
	}
	Discord struct {
		Token        string
		DefaultAdmin int64
	}
	Kairos struct {
		AppID  string
		APIKey string
	}
	Twitter struct {
		ConsumerKey       string
		ConsumerSecret    string
		AccessToken       string
		AccessTokenSecret string
	}
}

// Galleries is all of follow.toml in a map
// It is a collection of Galleries
type Galleries struct {
	Gallery map[string]Gallery
}

// Gallery is a collection of Subjects
type Gallery struct {
	Gallery  string
	Server   int64
	Subjects map[string]Subject
}

// Subject is the base subject config
type Subject struct {
	Name     string
	Channel  int64
	Follow   []string
	Hashtags []string
	Lists    []int64
}

// Recognize is supposed to be used for passing Twitter data to Kairos
type Recognize struct {
	URL          string
	AccountOrTag string
	SubjectName  string
}

// HashMessage is used for storing the message ID and its image hash
type HashMessage struct {
	Hash    *goimagehash.ImageHash
	Message *discordgo.Message
}
