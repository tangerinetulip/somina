/*
 *	 somina - A Discord bot for posting images from Twitter, with facial recognition.
 *
 *   Copyright (C) 2017 tangerinetulip
 *
 *   somina is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   somina is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"github.com/bwmarrin/discordgo"
	"github.com/getsentry/raven-go"
	"github.com/sirupsen/logrus"

	"github.com/tangerinetulip/anaconda"
	"github.com/tangerinetulip/somina/tulips"
)

func main() {
	// Set Logrus log level
	tulips.SetLogLevel()

	// Set DSN for Raven-Sentry
	errRaven := raven.SetDSN(tulips.GetSentryDSN())
	if errRaven != nil {
		logrus.Warn("Failed to set Sentry DSN.")
	}
	errHelp(errRaven)

	// Create default config files if configured ones don't exist yet
	tulips.CreateConfigSamples()

	// Create folders for downloading images into, if option is turned on
	tulips.CreateDirectories()

	// Create a new Discord session using the provided bot token.
	discord, errDiscordSession := discordgo.New("Bot " + tulips.GetDiscordToken())
	if errDiscordSession != nil {
		logrus.Fatal("Failed to create Discord session!")
	}
	errHelp(errDiscordSession)

	// Get the account information.
	discordUser, errDiscordUser := discord.User("@me")
	if errDiscordUser != nil {
		logrus.Error("Failed to get Discord user.")
	}
	errHelp(errDiscordUser)

	// Store the account ID for later use.
	botID := discordUser.ID
	logrus.WithFields(logrus.Fields{"Bot ID": botID}).Info("Starting...")

	// Open the websocket and begin listening.
	errDiscordOpen := discord.Open()
	if errDiscordOpen != nil {
		logrus.Fatal("Failed to open Discord socket!")
	}
	errHelp(errDiscordOpen)

	// Twitter API set-up from config file
	client := anaconda.NewTwitterApi(
		tulips.GetAccessToken(),
		tulips.GetAccessSecret(),
		tulips.GetConsumerKey(),
		tulips.GetConsumerSecret())

	// Sets up Kairos App ID and API key
	tulips.SetKairosID()
	tulips.SetKairosKey()

	// Stores Discord session and Twitter client
	tulips.SetDiscordSession(discord)
	tulips.SetTwitterClient(client)

	// Start Twitter loops and initialize Discord handlers
	logrus.Info("Starting loops and handlers...")
	go tulips.CheckAllTwitter()
	discord.AddHandler(tulips.HandleMessages)

	// Simple way to keep program running until CTRL-C is pressed.
	<-make(chan struct{})
}

func errHelp(err error) {
	if err != nil {
		raven.CaptureError(err, nil)
		return
	}
}
